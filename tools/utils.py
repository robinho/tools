# -*- coding: utf-8 -*-
import hashlib
import uuid


def generate_password(ad=False):
    if ad:
        return str(str(uuid.uuid4()).encode('ascii'))
    else:
        return str(hashlib.md5(str(uuid.uuid4()).encode('ascii')).hexdigest())


def is_number(number):
    try:
        int(number)
        return True
    except ValueError:
        return False
