# -*- coding: utf-8 -*-
import decimal
from json import dumps
from datetime import datetime


def handler(dct: dict):
    """
        Convert a dict to be compliance to Json Format
    """

    def converter(o):
        if isinstance(o, datetime) or isinstance(o, decimal.Decimal):
            # noinspection PyArgumentList
            return o.__str__()

    if isinstance(dct, dict):
        return dumps(dct, default=converter, ensure_ascii=False, sort_keys=True, indent=4)
    else:
        raise TypeError
