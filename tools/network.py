# -*- coding: utf-8 -*-
import ipaddress
import re


IP_ADDRESS = r'[0-9]+(?:\.[0-9]+){3}'


def is_ip(ip_address: str, verbose: bool = False):
    result = False
    data = None
    try:
        data = ipaddress.ip_address(ip_address)
        result = True

    except ValueError as err:
        data = str(err)

    finally:
        if verbose:
            return result, data
        else:
            return result


def parse_ip(text: str):
    """
        Locate IP Like Address in a given text

        Returns
        -------
        result: List
            List of IP Addresses

    """
    aux = re.findall(IP_ADDRESS, text)
    ip_list = []
    for ip in aux:
        res = is_ip(ip)
        if res:
            ip_list.append(ip)

    return list(set(ip_list))


def is_usp(host):
    """
        Verify if a given host (in text format) is part of the USP range of IPs

        host: str
            Host IP Address to be checked (ipv4 or ipv6)
    """
    hosts_usp = ['143.107', '200.144', '200.136', '2001:12d0']
    for subnet in hosts_usp:
        if str(host).startswith(subnet):
            return True
    return False
