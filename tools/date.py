# -*- coding: utf-8 -*-
import datetime
from dateutil.parser import parse


def epoch_to_date(epoch):
    output = int(epoch) / 1000.0
    return datetime.datetime.fromtimestamp(output).strftime('%Y-%m-%d %H:%M:%S.%f')


def ms_epoch(date):
    epoch = datetime.datetime.utcfromtimestamp(0)
    result = (date - epoch).total_seconds() * 1000.0
    return int(result)


def string_to_epoch(date: str):
    return (parse(date) - datetime.datetime(1970, 1, 1)).total_seconds()


def ms_date(ms, extended=180):
    # works using BRT
    dt = parse(str(ms))
    if extended != 0:
        dt = dt + datetime.timedelta(days=extended)
    return ((int(dt.timestamp()) - 10800) * 10000000) + 116444736000000000


def date_to_string(o: object) -> object:
    if isinstance(o, datetime.datetime):
        return o.__str__()
