# -*- coding: utf-8 -*-
import re
import unicodedata
from unicodedata import normalize

EMAIL = r'^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$'


def is_text(text):
    try:
        if "'" in text:
            text = text.replace("'", "")
        s = unicodedata.normalize('NFD', text.replace(' ', '')).encode('ascii', 'ignore')

        return s.isalpha()

    except TypeError:
        return False


def is_email(email):
    try:
        return True if re.search(EMAIL, email) else False

    except TypeError:
        return False


def remove_acentos(text: str):
    return normalize('NFKD', text).encode('ASCII', 'ignore').decode('ASCII')


def lreplace(obj: tuple, string: str):
    """
        Modify a text object changing the left part with a given string
    """
    return obj[1] + string[len(obj[0]):] if string.startswith(obj[0]) else string


def rreplace(obj: tuple, string: str):
    """
        Modify a text object changing the right part with a given string
    """
    return string[:-len(obj[0])] + obj[1] if string.endswith(obj[0]) else string


def remove_duplicate(string: str):
    _aux = ''
    output = ''
    for letter in string:
        if not letter == _aux:
            output += letter
            _aux = letter

    return output


def fonetica(text: str):
    """
        Convert a given string to his "USP Fonetica" version
    """

    def processor(word: str):

        word = remove_duplicate(word)

        startswith = {"WA": "VA", "WE": "VO", "WO": "VO", "WU": "VU", "WI": "UI", "SQU": "ISQ", "SQ": "ISQ", "W": ""}
        for sw in startswith:
            word = lreplace((sw, startswith[sw]), word)

        endswith = {"OES": "N", "ONS": "N", "OIM": "N", "UIM": "N", "EIA": "IA", "AM": "AN", "AO": "AN",
                    "OM": "ON", "TH": "TE", "N": "M", "X": "IS", "D": "", "B": "", "T": "", "L": "O"}
        for ew in endswith:
            word = rreplace((ew, endswith[ew]), word)

        anyplace = {"GN": "N", "MN": "N", "TSCH": "X", "TCH": "X", "SCH": "X", "TSH": "X", "SH": "X", "CH": "X",
                    "LH": "LI", "NH": "N", "PH": "F", "H": "", "SCE": "SE", "SCI": "SI", "SCY": "SY", "CS": "S",
                    "KS": "S", "PS": "S", "TS": "S", "TZ": "S", "XS": "S", "CE": "SE", "CI": "SI", "CY": "SY",
                    "GE": "JE", "GI": "JI", "GY": "JY", "GD": "D", "CK": "Q", "PC": "Q", "QU": "Q", "SC": "SQ",
                    "SK": "SQ", "XC": "SQ", "CT": "T", "GT": "T", "PT": "T"}
        for ap in anyplace:
            word = word.replace(ap, anyplace[ap])

        lnv = {"U": r"L[^AEIOU]", "M": r"N[^AEIOU]"}
        for ln in lnv:
            m = re.findall(lnv[ln], word)
            if len(m) > 0:
                for i in m:
                    word = word.replace(i, ln + i[1:])

        consonants = {"B": "P", "K": "C", "Q": "C", "T": "D", "E": "I", "Y": "I",
                      "V": "F", "W": "F", "U": "O", "Z": "S"}
        for c in consonants:
            word = word.replace(c, consonants[c])

        word = remove_duplicate(word)

        return word

    if not isinstance(text, str):
        raise TypeError

    o = text.upper()
    data = {"'": "", "Ç": "S", "Ñ": "N"}
    for elem in data:
        o = o.replace(elem, data[elem])

    o = normalize('NFKD', o).encode('ASCII', 'ignore').decode('ASCII')

    data = [" DA ", " DE ", " DI ", " DO ", " DU ", " E ", " DOS ", " DAS "]
    for elem in data:
        o = o.replace(elem, " ")

    words = [processor(word) for word in o.split(" ")]
    return words
