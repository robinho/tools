from tools.jsonutils import *
import pytest
import datetime
import decimal


# noinspection PyTypeChecker
def test_handler():
    assert handler({'test': 'test'})
    with pytest.raises(TypeError):
        handler('test')
    assert handler({'date': datetime.datetime.now()})
    assert handler({'decimal': decimal.Decimal(10)})
    assert handler({'decimal': decimal.Decimal(10)}) == '{\n    "decimal": "10"\n}'
