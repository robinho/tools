# -*- coding: utf-8 -*-
from tools.text import *
import pytest


# noinspection PyTypeChecker
def test_is_text():
    assert is_text(1) is False
    assert is_text('text') is True
    assert is_text("D'artagnan") is True


# noinspection PyTypeChecker
def test_is_email():
    assert is_email('eisinger@usp.br') is True
    assert is_email('not_a_email') is False
    assert is_email(1) is False


# noinspection PyTypeChecker
def test_remove_acentos():
    assert remove_acentos('sem_acentos') == 'sem_acentos'
    assert remove_acentos('é ação') == 'e acao'


# noinspection PyTypeChecker
def test_fonetica():
    assert fonetica('Robson Eisinger')
    assert fonetica('Robson Eisinger') == ['ROPSOM', 'ISIMJIR']
    with pytest.raises(TypeError):
        fonetica(1)
