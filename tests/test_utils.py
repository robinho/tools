# -*- coding: utf-8 -*-
from tools.utils import *


def test_is_number():
    assert is_number('x') is False
    assert is_number(1) is True


def test_generate_password():
    assert generate_password()
    assert generate_password(ad=True)
