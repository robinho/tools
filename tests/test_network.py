# -*- coding: utf-8 -*-
from tools.network import *
import pytest


# noinspection PyTypeChecker
def test_is_ip():
    assert is_ip('200.144.231.10') is True
    assert is_ip('not_ip') is False
    assert is_ip('not_ip', True)
    assert is_ip('200.144.231.10', True)


# noinspection PyTypeChecker
def test_parse_ip():
    assert parse_ip('tem ip 200.144.231.10')
    with pytest.raises(TypeError):
        parse_ip(1)


# noinspection PyTypeChecker
def test_is_usp():
    assert is_usp('200.144.231.10') is True
    assert is_usp('192.168.1.3') is False
    assert is_usp(1) is False

