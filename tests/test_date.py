# -*- coding: utf-8 -*-
from tools.date import *
import pytest
import datetime


def test_date_to_string():
    now = datetime.datetime.now()
    assert date_to_string(now) == str(now)
    assert date_to_string(1) is None
    with pytest.raises(Exception):
        assert date_to_string('not_date')
    with pytest.raises(Exception):
        assert date_to_string(1)


def test_ms_epoch():
    assert ms_epoch(datetime.datetime.now())
    with pytest.raises(Exception):
        assert ms_epoch('not_date')


def test_epoch_to_date():
    assert epoch_to_date(ms_epoch(datetime.datetime.now()))
    with pytest.raises(Exception):
        assert epoch_to_date('not_date')


def test_string_to_epoch():
    assert string_to_epoch('2021-09-12 09:55:11')
    assert string_to_epoch('2021-09-12 09:55:11') == 1631440511.0


def test_ms_date():
    assert ms_date('2009-06-15T13:45:30')
